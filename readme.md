# Fmincon toolbox

 Purpose
 -------
 
 The goal of this toolbox is to provide a fmincon function in Scilab.
 The fmincon function is a linearly and nonlinearily constrained optimization
 solver.
 Currently, we use ipopt for the actual solver of fmincon but other solvers could
 be added in the future.
  
 We provide the optimoptions function (which superseeds the former optimset),
 which manage options which are required by fmincon.
  
 The current implementation is able to manage the following use cases. By default
 we use a L-BFGS formula in order to compute an approximate of the Hessian of the
 Lagrangian.
  
  (0) The initial guess is provided in the x0 input argument.
  
  (1) The nonlinear objective function and 
  the nonlinear constraints are provided.
  The fun and nonlcon function can be customized to  configure the nonlinear
 objective function and nonlinear constraints.
  In this case, we use order two finite differences with optimal step size in
 order to compute the gradient of the objective function and the gradient of the
 constraints.
  
  (2) The parameters are subject to bounds.
  The lb and ub parameters can be configure to set 
  bounds on the parameters.
 
  (3) Linear equalities and linear inequalities are  managed
   
  (4) The objective function and constraints function can 
  provide the exact gradients as additionnal output arguments
  of their function definition.
 
  The two &quot;SpecifyObjectiveGradient&quot; and
 &quot;SpecifyConstraintGradient&quot; options can be turned 
  on for that purpose.
 
  (5)  Efficient approximation of the sparse Hessian by finite differences taking
 into account the sparsity pattern is now possible (1.0.2 feature)

Features
--------

 * fmincon : Solves a linearly and/or nonlinearily constrained optimization problem.
 * optimoptions : Configures and returns an updated optimization data structure.

Authors
-------

Copyright (C) 2020-2024 - Stephane Mottelet

Copyright (C) 2010 - DIGITEO - Michael Baudin

Licence
-------

This toolbox is released under the GPL licence :

https://www.gnu.org/licenses/gpl-3.0.html

