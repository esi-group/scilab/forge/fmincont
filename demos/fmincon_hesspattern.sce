
// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function [f,g]=fungrad(x,A,b)
    Ax= A*x;
    f = .5*sum(x.*Ax)-sum(b.*x);
    if argn(1)==2
        g = Ax-b;
    end
end

grand("setsd",13)
rand("seed",11)

n = 1000;
A = sprand(n,n,0.01);
A=A*A';
b = rand(n,1);

opt = optimoptions("fmincon");
opt.SpecifyObjectiveGradient = %t;
opt.Display = "iter";

opt.HessianApproximation = "finite-difference";
opt.FiniteDifferenceType = "central";
opt.HessPattern = A;

problem =  struct();
problem.objective = list(fungrad,A,b);
problem.x0 = ones(n,1);
problem.lb = -ones(n,1);
problem.ub = ones(n,1);
problem.options = opt;

tic
x=fmincon(problem);
disp(toc())
