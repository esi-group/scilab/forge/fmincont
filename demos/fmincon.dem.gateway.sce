// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2021 - UTC - Stephane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

demopath = get_absolute_file_path("fmincon.dem.gateway.sce");
subdemolist = [
"Nonlinear example", "case1.sce"; ..
"Rosenbrock Post Office Problem", "fmincon_linear.sce"; ..
"Fitting problem", "fmincon_fit.sce"; ..
"Reservoir", "reservoir.sce";...
"Reactors", "reactors.sce";...
"Optimization of chemical process", "temperature.sce";...
"Large scale problem", "fmincon_quad.sce";...
"Minimal surface", "fmincon_minimal_surface.sce"
];
subdemolist(:,2) = demopath + subdemolist(:,2)
