// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2021 - UTC - Stephane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function fmincon_gradjac()

// A case where we provide the gradient of the objective
// function and the Jacobian matrix of the constraints.
// The objective function and its gradient
function [f, G]=objfungrad(x)
[lhs,rhs]=argn()
f = exp(x(1))*(4*x(1)^2+2*x(2)^2+4*x(1)*x(2)+2*x(2)+1)
if ( lhs  > 1 ) then
G = [
f + exp(x(1)) * (8*x(1) + 4*x(2))
exp(x(1))*(4*x(1)+4*x(2)+2)
]
end
endfunction
// The nonlinear constraints and the Jacobian
// matrix of the constraints
function [c, ceq, DC, DCeq]=confungrad(x)
// Inequality constraints
c(1) = 1.5 + x(1) * x(2) - x(1) - x(2)
c(2) = -x(1) * x(2)-10
// No nonlinear equality constraints
ceq=[]
[lhs,rhs]=argn()
if ( lhs > 2 ) then
// DC(:,i) = gradient of the i-th constraint
// DC = [
//   Dc1/Dx1  Dc2/Dx1
//   Dc1/Dx2  Dc2/Dx2
//   ]
DC= [
x(2)-1, -x(2)
x(1)-1, -x(1)
]
DCeq = []
end
endfunction
// Test with both gradient of objective and gradient of constraints
options = optimoptions("fmincon","SpecifyObjectiveGradient",%t,"SpecifyConstraintGradient",%t,"Display","iter");
// The initial guess
x0 = [-1,1];
// The expected solution : only 4 digits are guaranteed
xopt = [-9.547345885974547   1.047408305349257]
fopt = 0.023551460139148
// Run fmincon
[x,fval,exitflag,output] = fmincon(objfungrad,x0,[],[],[],[],[],[], confungrad,options)
endfunction

fmincon_gradjac()
clear fmincon_gradjac
