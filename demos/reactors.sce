// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function reactors()

function out=cost(X)
  out=X($); // C(N)
end

function [c,ceq]=nlcon(X,q,k,n,C0)
  c=[];
  N=length(X)/2;
  V=X(1:N);
  C=X(N+1:$);
  //State equations
  ceq(1)=q*(C0-C(1))-k*V(1)*C(1)^n;
  for i=2:length(C)
    ceq(i)=q*(C(i-1)-C(i))-k*V(i)*C(i)^n;
  end
end

clf
subplot(2,1,1)

titlepage("$\mathop{\operatorname{minimize}}_{C,V \in \mathbb{R}^n}f(C,V)=C_n,\\\begin{array}{ll}&V_i\geq 0,\,C_i\geq 0,\,i=1\dots n,\\&\sum_{i=1}^n V_i=20,\\ &q(C_{i-1}-C_i)-kV_iC_i^\alpha=0,\,i=1\dots n\end{array}$")
gca().box = "off";

N=10; // number of reactors
q=71/3600;
k=0.00625;
n=2.5;
C0=10;

Vini=20/N*ones(N,1);
Cini=C0*ones(N,1);
Xini=[Vini;Cini];
Aeq=[ones(1,N) zeros(1,N)];
beq=20;

options = optimoptions('fmincon','display','iter',"FiniteDifferenceType","complexstep");

options.ConstraintTolerance=1e-12;

X=fmincon(cost,Xini,[],[],Aeq,beq,...
  zeros(2*N,1),%inf*ones(2*N,1),list(nlcon,q,k,n,C0),options)
V=X(1:N);
C=X(N+1:$);
subplot(2,2,3)
bar(V,color="#0000FF")
title('V :Volumes of reactors')
subplot(2,2,4)
bar(C,color="#00A000")
title('C : Concentrations')

demo_viewCode("reactors.sce")
endfunction

reactors()
clear reactors
