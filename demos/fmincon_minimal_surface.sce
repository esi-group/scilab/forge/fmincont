//
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
// Copyright (C) 2020-2021 - UTC - Stephane MOTTELET
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
//--------------------------------------------------------------------------

function fmincon_minimal_surface(n)
    function [S,dsdf] = fungrad(f)
        // compute the surface and its gradient using the triangulation
        global funcount
        z2mz1 = I21*f;
        z3mz1 = I31*f;
        px =  y2my1.*z3mz1 - y3my1.*z2mz1;
        py = -x2mx1.*z3mz1 + x3mx1.*z2mz1;
        sq = sqrt(px.*px+py.*py+pz2);
        S = sum(sq);
        if argn(1) > 1
            // compute gradient by reverse differentiation of S
            dsdpx = px./sq; dsdpy = py./sq;
            dsdz3mz1 =  dsdpx.*y2my1 - dsdpy.*x2mx1;
            dsdz2mz1 = -dsdpx.*y3my1 + dsdpy.*x3mx1;
            dsdf = (dsdz2mz1'*I21 + dsdz3mz1'*I31)';
        end
        funcount = funcount+1;
    end
    function stop = stopcallback(x, optimValues, state)
        global fmincon_hdl
        stop = %f;
        if state=="init"
            fmincon_hdl.Userdata = %f;
            fmincon_hdl.Enable = "on"
        elseif state == "iter"
            stop = fmincon_hdl.Userdata;
            if stop
                fmincon_hdl.Enable = "off";                
            end
         elseif state == "done"
            fmincon_hdl.Enable = "off"; 
        end
    endfunction
    function optimize_and_plot(problem,opt,title_text,isub)
        global funcount
        drawlater
        ax = newaxes()
        ax.axes_bounds=[(isub-1)/3 0.15 1/3 0.8];
        ax.margins(3)=0.2
        ax.data_bounds = [min(x) max(x) min(x) max(x) min(cnd) max(cnd)]
        title(ax,[title_text]);
        param3d(X(bdy),Y(bdy),cnd(bdy),-125,51)
        gce().thickness=2;
        gce().foreground=color("red")   
        isoview on
        drawnow
        problem.options = opt;
        tic
        funcount = 0;
        [f,S] = fmincon(problem);
        duration = toc();

        drawlater 
        title([title_text
               msprintf("Surface = %f",S)
               msprintf("Time = %f",duration)
               msprintf("Function calls = %d",funcount)])
        surf(x,x,matrix(f,n,n),"facecolor","interp")
        isoview on
        drawnow
    endfunction
    
    // define the mesh the (x,y) domain
    N = n^2;
    x = linspace(-1,1,n);
    [X,Y] = meshgrid(x,x);
    X = X(:);
    Y = Y(:);
    [tri,bdy] = mesh2d(X(:),Y(:));

    // helper matrices and vectors for normals computation
    nt = size(tri,2);
    I21 = sparse([1:nt 1:nt; tri(1,:) tri(2,:)]',[-ones(1,nt), ones(1,nt)],[nt,N])
    I31 = sparse([1:nt 1:nt; tri(1,:) tri(3,:)]',[-ones(1,nt), ones(1,nt)],[nt,N])
    x2mx1 = I21*X;
    x3mx1 = I31*X; 
    y2my1 = I21*Y; 
    y3my1 = I31*Y;
    pz2 = (x2mx1.*y3my1-y2my1.*x3mx1).^2;

    // Hessian pattern is a little bit larger than the laplacian pattern
    d1x=sparse(ones(n-1,1));
    d0x=sparse(ones(n,1));
    grad = (-diag(d1x,-1) + diag(d1x,1) );
    gradx = grad .*. speye(n,n);
    grady = speye(n,n) .*. grad;
    lap = (diag(d1x,-1)+diag(d1x,1)-2*diag(d0x));
    lapxy = lap .*. speye(n,n) + speye(n,n) .*. lap+gradx*grady;
    
    // Dirichlet boundary condition
    cnd = sign(sin(4*(X+Y)))/2;
        
    lb = -ones(N,1);
    lb(bdy) = cnd(bdy);
    ub = ones(N,1)
    ub(bdy) = cnd(bdy);

    problem = struct();
    problem.objective = fungrad;
    problem.ub = ub;
    problem.lb = lb;
    problem.x0 = zeros(N,1);
    
    opt = optimoptions("fmincon");
    opt.outputFcn = stopcallback;
    opt.Display="iter";
    
    clf
    gcf().figure_size = [958,567]
    gcf().color_map = parulacolormap(128);

    gca().axes_bounds = [0 0 1 0.15];
    titlepage("$\operatorname{minimize}_f \int_{D}\sqrt{1+\Vert\nabla f\Vert^2}\,dx\,dy,\quad +\mbox{boundary conditions}$")
    gca().box = "off";
    demo_viewCode("fmincon_minimal_surface.sce")
    
    button_text = msprintf("Problem has %d unknowns. Click to stop optimization",N);
    fmincon_hdl=uicontrol("style","pushbutton","string",button_text,...
    "units","normalized","position",[0 0 1 0.075],"tag","stopbutton",...
    "callback","gcbo.Userdata=%t","callback_type",10,"Userdata",%f)

    opt.OptimalityTolerance = 1e-3;
    optimize_and_plot(problem,opt,"FD Gradient, BFGS Hessian",1);

    opt.SpecifyObjectiveGradient=1;
    opt.OptimalityTolerance = null();
    optimize_and_plot(problem,opt,"Exact gradient, BFGS Hessian",2);

    opt.HessianApproximation = "finite-difference";
    opt.HessPattern=lapxy;
    //opt.checkGradients=%t;
    optimize_and_plot(problem,opt,"Exact gradient, Sparse FD Hessian",3);
    

end

fmincon_minimal_surface(21)
clear fmincon_minimal_surface
clearglobal fmincon_hdl