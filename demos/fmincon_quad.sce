// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function fmincon_quad(n)

function [f,g]=fun(x,A,b)
    Ax = A*x;
    f =.5*x'*Ax-b'*x;
    g = Ax-b;
endfunction    

function [c,ceq,dc,dceq]=constr(x)
    n = length(x);
    c = x(1:2:$).^2+x(2:2:$).^2-1;
    ceq =[];
    dc = 2*sparse([1:n;(1:n/2).*.[1 1]]',x);
    dceq = [];
endfunction    


function h=hes(x,lambda,A)
    n = length(x);
    h = A + sparse([1:n;1:n]',lambda.ineqnonlin.*.[2;2])
endfunction

function plotpoints(x,optimValues,state,user_text)
    global fmincon_hdl
    n = size(x,1)
    drawlater
    if state == "init" then
        fmincon_hdl = plot(x(1:2:$),x(2:2:$),'o')
        xlabel("$\Large x_{2i-1}$")
        ylabel("$\Large x_{2i}$")
        gca().data_bounds=[-1,1,-1,1];
        fmincon_hdl.mark_size=2;
        t = linspace(0,2*%pi,256);
        plot(cos(t),sin(t),"r","thickness",2)
        isoview
    elseif state == "iter"
        fmincon_hdl.data = [x(1:2:$),x(2:2:$)];
        hdl = fmincon_hdl;
        while hdl.type <> "Axes"
            hdl = hdl.parent;
        end
        title(hdl,[
        msprintf("n=%4d unknowns, residual is %f",n,optimValues.fval)
        user_text
        ""
        ]);
    end
    drawnow
endfunction

// build a tridiagonal matrix
ij = [1:n 1:n-1
      1:n 2:n]';
v = grand(2*n-1,1,"nor",0,4);
a = sparse(ij,v);

a=a*a';
b = grand(n,1,"nor",0,1);

problem = struct();
problem.objective=list(fun,a,b);
problem.nonlcon=constr;
problem.x0 = %eps*grand(n,1,"nor",0,1);

opt = optimoptions("fmincon");
opt.Display = "iter";
opt.SpecifyObjectiveGradient=%t;
opt.SpecifyConstraintGradient=%t;
opt.ConstraintTolerance = 1e-1;
opt.OptimalityTolerance = 1e-1;

clf
demo_viewCode("fmincon_quad.sce")

titlepage("$\begin{array}{l}\operatorname{minimize}_x f(x) = \frac{1}{2}x^\top A x -b^\top x\\\mbox{constraints : }x_{2i-1}^2+x_{2i}^2 \leq 1,\;i=1\dots \frac{n}{2}\end{array}$")
gca().box = "off";
gca().axes_bounds = [0 0 1 .25]

// Limited memory BFGS Hessian
subplot(2,2,3)
gca().axes_bounds=[0 0.25 .5 .75];
opt.PlotFcn = list(plotpoints,"Limited memory BFGS Hessian");
problem.options = opt;
tic
x=fmincon(problem);
gca().title.text = [gca().title.text;msprintf("Time = %4.2f s",toc())]

// True sparse Hessian
subplot(2,2,4)
gca().axes_bounds=[.5 0.25 .5 .75];
opt.PlotFcn = list(plotpoints,"Exact sparse Hessian");
opt.HessianFcn=list(hes,a);
problem.options = opt;
tic
x=fmincon(problem);
gca().title.text(3) = msprintf("Time = %4.2f s",toc());


endfunction

fmincon_quad(2500)
clear fmincon_quad
clearglobal fmincon_hdl