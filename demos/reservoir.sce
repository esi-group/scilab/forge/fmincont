// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function reservoir()

function out=cost(x,c)
  d=x(1);
  L=x(2);
  out = %pi*(c(1)*d*L+c(2)*d^2/2);
end
function [cineq,ceq]=constr(x,V)
  d=x(1);
  L=x(2);
  cineq = [];
  ceq = %pi*d^2*L/4-V;
end
function plotfun(x,varargin)
    d=x(1);
    L=x(2);
    drawlater
    clf
    [TH,R]=meshgrid(linspace(0,2*%pi,32),[0 d/2]);
    mesh(R.*cos(TH),R.*sin(TH),zeros(R))
    mesh(R.*cos(TH),R.*sin(TH),zeros(R)+L)
    mesh(d/2*cos(TH),d/2*sin(TH),R/(d/2)*L)
    gca().data_bounds=[-5 5 -5 5 0 10]
    isoview on
    drawnow
end

V=330;
c=[1 1];
problem = struct();
problem.x0 = rand(2,1);
problem.lb = [0;0];
problem.ub = [10;10];
problem.nonlcon = list(constr,V);
problem.objective = list(cost,c);
problem.options = optimoptions('fmincon','display','iter','PlotFcn',plotfun);
x = fmincon(problem)

demo_viewCode("reservoir.sce")

endfunction

reservoir()
clear reservoir
