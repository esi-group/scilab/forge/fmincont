// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function [st,sthid,solver,empty_content] = %optim_options_defaults(opt)
    empty_content = size(opt.contents,"*") == 0 || isempty(fieldnames(opt.contents))
    solver = strsubst(typeof(opt),"optim_options_","");
    if empty_content == %f && isfield(opt.contents,"Algorithm") then
        execstr(msprintf("[st,sthid] = %%optim_options_%s(""%s"")",solver,opt.contents.Algorithm));        
    else
        execstr(msprintf("[st,sthid] = %%optim_options_%s(""%s"")",solver,"default"));
    end
endfunction