// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_function(prop,value,values)
    if and(typeof(value) <> ["function","list"]) then
        message = msprintf("Invalid value for %s option: a function or a list is expected.",prop);
        error(message)       
    else
        if typeof(value) == "list"
            if typeof(value(1)) <> "function"
                error(msprintf("First element of list must be a function."));
            end
            vars = macrovar(value(1));
        else
            vars = macrovar(value);
        end
        if prop == "outputfcn"
            if size(vars(1),"*") < 3 || size(vars(2),"*") < 1
                message = msprintf("Invalid function for %s option: a function with at least 3 input arguments and 1 output argument is expected.",prop);
                error(message)
            end
        elseif prop == "plotfcn"
            if size(vars(1),"*") < 3
                message = msprintf("Invalid function for %s option: a function with at least 3 input arguments is expected.",prop);
                error(message)
            end
        end
        out = value;              
    end
end
