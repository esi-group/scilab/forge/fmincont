// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_hessianapproximation(prop,value,values)
    if typeof(value) == "string" && size(value,"*") == 1
        if or(convstr(value)==["bfgs","lbfgs","finite-difference"])
            out = convstr(value)
           return
        end
    end
    if typeof(value) == "ce" && value{1} == "lbfgs" && typeof(value{2}) == "constant" && max(0,value{2}) == floor(value{2})
        out = value;                
    else
        message = msprintf("Invalid value for HessianApproximation option: valid values are ""bfgs"",""finite-difference"",""lbfgs"",{""lbfgs"",n}");
        error(message)
    end
end
