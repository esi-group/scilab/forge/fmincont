// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function opt = %i_optim_options(i,value,opt_in)
    [st, sthid, solver, algo] = %optim_options_defaults(opt_in)
    k = find(convstr(fieldnames(st)) == convstr(i));
    khid = find(convstr(fieldnames(sthid)) == convstr(i));
    if ~isempty(k)
        prop = fieldnames(st)(k);
        values = st(prop);
    elseif ~isempty(khid)
        prop = fieldnames(sthid)(khid);
        values = sthid(prop);
    else
        error(msprintf("Field ""%s"" does not exists in %s options\n\n",i,solver))
    end
    opt = opt_in;
    stout = opt.contents;
    // deletion of field
    if typeof(value) == "listdelete" then
        stout(prop) = null();
        opt.contents = stout;
        return
    else
        // verify validity of value to be inserted
        setFun = msprintf("%%optim_options_%s_%s_set_%s",solver,st.Algorithm,convstr(i)); // solver_algo_set_prop
        if ~exists(setFun) then
            setFun = msprintf("%%optim_options_%s_set_%s",solver,convstr(i)); // solver_set_prop
            if ~exists(setFun) then
                setFun = msprintf("%%optim_options_set_%s",convstr(i)); // set_prop
                if ~exists(setFun) then
                    setFun = msprintf("%%optim_options_set_%s",typeof(values)); // set_type (type of default values)
                end
            end
        end
    end
    try
        execstr(msprintf("stout(prop) = %s(prop,value,values)",setFun));            
    catch
        error(lasterror())
    end        
    opt.contents = stout;
endfunction
