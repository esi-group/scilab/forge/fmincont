// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source w is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function out = %optim_options_set_tolerance(prop,value,values)
    if typeof(value) <> "constant" then
        message = msprintf("Invalid value for %s option: a positive real is expected.",prop);
        error(message)       
    elseif value <= %eps
        message = msprintf("Invalid value for %s option: value cannot be less than relative machine precision %e.",prop,%eps);
        error(message)
    else
        out = value;              
    end
end