// Copyright (C) 2020-2023 - UTC - Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function opt = optimoptions(varargin)
    [lhs,rhs] = argn();
    if rhs >= 1 then
        _typ = typeof(varargin(1));
        if strindex(_typ,"optim_options") == 1
            opt = varargin(1);
        elseif or(_typ == ["string" "function"])
            solver = varargin(1);
            if _typ == "function"
                solver = macr2tree(solver)(2);
            end
            try
                opt = evstr("%optim_options_"+solver+"()");
            catch
                error(msprintf(_("%s: No default options available: the function ''%s'' does not exist on the path."), "optimoptions", solver))
            end
        else
            error(msprintf(_("%s: argument %d must be a string, a function or an options list."), "optimoptions", 1))
        end
        st = %optim_options_defaults(opt);
        fields = fieldnames(st);
        for i = 2:2:length(varargin)        
            if typeof(varargin(i)) <> "string"
                error(msprintf(_("%s: wrong type for argument %d, a string expected"), "optimoptions", i));
            end
            prop = varargin(i);
            k = find(strstr(convstr(fields),convstr(prop)) == convstr(fields));
            if isempty(k)
                error(msprintf(_("%s: field ""%s"" does not exists in %s options\n\n"),"optimoptions",prop,solver))
            end
            if length(k) > 1
                message = msprintf(_("%s: string ""%s"" matches multiple options: "),"optimoptions",prop,solver);
                for i=1:length(k)
                    message = message + msprintf( """%s""",fields(k(i)));
                    if i < size(k,"*")
                        message = message + ","
                    end
                end
                error(message)
            end
            if i == length(varargin)
                error(msprintf("%s: missing value for Field ""%s""\n\n","optimoptions", fields(k)))
            end
            try
                opt = %i_optim_options(fields(k),varargin(i+1),opt);
            catch
                error(lasterror())
            end
        end
    else
        errmsg = msprintf(_("%s: not enough input arguments, at least %d expected"), "optimoptions", 1)
        error(errmsg)
    end
endfunction
