// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2020-2021 Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

path_makedist = get_absolute_file_path("makedist.sce");
cd(path_makedist);

modulename = "fmincon"
version = mgetl("VERSION")
[sci,v]=getversion()
http_get(msprintf("%s/%s/%s/DESCRIPTION","https://atoms.scilab.org/toolboxes",modulename,version),"DESCRIPTION");

filename = "fmincon_"+mgetl("VERSION")+".bin.zip"
cd ..
compress(filename,"fmincont",format="zip");
cd(path_makedist);
